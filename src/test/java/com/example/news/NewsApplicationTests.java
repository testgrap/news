/*
package com.example.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@ActiveProfiles(value = "test")
@Testcontainers
@DirtiesContext
@Transactional
@AutoConfigureMockMvc
@SpringBootTest(classes = NewsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NewsApplicationTests {

    @Autowired
    protected MockMvc mockMvc;

    @Container
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer()
            .withDatabaseName("secret")
            .withPassword("secret")
            .withUsername("secret");

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.username", container::getUsername);
    }

}
*/
