/*
package com.example.news.integration;

import com.example.news.NewsApplicationTests;
import com.example.news.entity.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class NewsServiceIT extends NewsApplicationTests {

    @BeforeEach
    void init() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/api/v1/author")
                .param("login", "testLogin")
                .param("name", "testName")
                .param("lastName", "testLastName")
                .param("patronymic", "testPatronymic")
        );
    }


    @Test
    void saveNewsTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/api/v1/news")
                .param("uri", "xxx-xxx-xxx-xxxx-xx.png")
                .param("title", "testTitle")
                .param("content", "testContent")
                .param("author", "testName")
                .param("status", Status.DRAFT.name())
        ).andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.id").value(1));
    }

}

*/
