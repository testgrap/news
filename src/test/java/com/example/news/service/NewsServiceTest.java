package com.example.news.service;

import com.example.news.entity.Author;
import com.example.news.entity.News;
import com.example.news.entity.Status;
import com.example.news.repository.NewsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NewsServiceTest {

    @InjectMocks
    private NewsService newsService;

    @Mock
    private NewsRepository newsRepository;

    private Author author;

    private News news;

    @BeforeEach
    void init() {
        author = Author.builder()
                .login("Test")
                .name("Test")
                .lastName("TestTest")
                .patronymic("MyPat")
                .build();

        news = News.builder()
                .id(1l)
                .urlImg("xxx-xxx-xxx-xxx.png")
                .title("titleTest")
                .status(Status.DRAFT)
                .content("contentTest")
                .dateTimeCreateNews(LocalDateTime.of(2000, 10, 22, 22, 22))
                .author(author)
                .build();
    }

    @Test
    void saveNews() {
        when(newsRepository.save(Mockito.any(News.class))).thenReturn(news);

        Long idNews = newsService.saveNews(news);

        assertEquals(1, idNews);

    }

    @Test
    void updateNews() {
    }

    @Test
    void getNews() {
    }

    @Test
    void getAllNews() {
    }
}