package com.example.news.dto;

import com.example.news.entity.Status;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NewsDto {

    private Long id;

    private Status status;

    private String date;

    private String uri;

    private String title;

    private String content;

    private String nameAuthor;
}
