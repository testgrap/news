package com.example.news.util;

import com.example.news.dto.NewsDto;
import com.example.news.entity.News;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class Convertor {


    public static List<NewsDto> mappingToNewsDto(List<News> newsList) {
        return newsList.stream()
                .map(news -> {
                    return NewsDto.builder()
                            .id(news.getId())
                            .title(news.getTitle())
                            .content(news.getContent())
                            .date(String.valueOf(news.getDateTimeCreateNews()))
                            .status(news.getStatus())
                            .uri(news.getUrlImg())
                            .nameAuthor(news.getAuthor().getName())
                            .build();
                }).collect(Collectors.toList());
    }

}
