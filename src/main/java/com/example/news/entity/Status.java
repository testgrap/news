package com.example.news.entity;

public enum Status {
    DRAFT(),
    PUBLISHED(),
    ARCHIVED();
}
