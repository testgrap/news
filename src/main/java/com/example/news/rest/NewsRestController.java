package com.example.news.rest;

import com.example.news.dto.NewsDto;
import com.example.news.entity.Author;
import com.example.news.entity.News;
import com.example.news.entity.Status;
import com.example.news.service.AuthorService;
import com.example.news.service.NewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api/v1/news")
public class NewsRestController {

    private final AuthorService authorService;

    private final NewsService newsService;

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");

    @PostMapping
    public Long saveNews(@RequestParam(value = "uri") String uri,
                         @RequestParam(value = "title") String title,
                         @RequestParam(value = "content") String content,
                         @RequestParam(value = "author") String nameAuthor,
                         @RequestParam(value = "status") Status status) throws Exception {

        Author existAuthor = authorService.isExistAuthor(nameAuthor);

        News newsCandidateOnSave = News.builder()
                .urlImg(uri)
                .title(title)
                .content(content)
                .author(existAuthor)
                .status(status)
                .dateTimeCreateNews(LocalDateTime.now())
                .build();

        return newsService.saveNews(newsCandidateOnSave);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> updateNews(@PathVariable(value = "id") Long id,
                                        @RequestParam(value = "status") Status status) {

        NewsDto newsDto = newsService.updateNews(id, status);

        return ResponseEntity.ok().body(newsDto);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getNews(@PathVariable(value = "id") Long id) {
        NewsDto news = newsService.getNews(id);
        return ResponseEntity.ok().body(news);
    }

    @GetMapping("/tape")
    public ResponseEntity<?> getAllNewsByParameters(@RequestParam(value = "status", required = false) Status status,
                                                    @RequestParam(value = "dateTime", required = false)
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startPublication) {

        List<NewsDto> allNews = newsService.getAllNews(status, startPublication);

        return ResponseEntity.ok().body(allNews);
    }
}