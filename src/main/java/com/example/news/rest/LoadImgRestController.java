package com.example.news.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/load")
public class LoadImgRestController {

    @PostMapping
    public String loadImg(@RequestParam(value = "file") MultipartFile file) throws IOException {

        File loadDirImg = new File("img");

        if (!loadDirImg.exists()) {
            loadDirImg.mkdir();
        }

        String uuidImg = UUID.randomUUID().toString();
        String pathToNameFile = "C://Users//Дмитрий//IdeaProjects//news//img" + "/" + uuidImg + "." + file.getOriginalFilename();
        file.transferTo(new File(pathToNameFile));
        return Stream.of(pathToNameFile.split("//|/")).reduce((x, y) -> y)
                .stream().collect(Collectors.joining());
    }

}
