package com.example.news.rest;

import com.example.news.entity.Author;
import com.example.news.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api/v1/author")
public class AuthorRestController {

    private final AuthorService authorService;

    @PostMapping
    public Long saveAuthor(@RequestBody Author author) {
        return authorService.saveAuthor(author);
    }


}
