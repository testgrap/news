package com.example.news.repository;

import com.example.news.entity.News;
import com.example.news.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface NewsRepository extends JpaRepository<News, Long> {

    @Query("select n from News n join fetch n.author where n.status = :status or n.dateTimeCreateNews >= :date")
    List<News> getAllByParameters(@Param(value = "status") Status status, @Param(value = "date") LocalDateTime localDateTime);
}
