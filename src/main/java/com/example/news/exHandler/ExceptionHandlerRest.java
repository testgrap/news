package com.example.news.exHandler;

import com.example.news.exHandler.exception.AuthorException;
import com.example.news.exHandler.exception.NewsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerRest {

    @ExceptionHandler(value = AuthorException.class)
    public ResponseEntity<?> authorException(AuthorException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(value = NewsException.class)
    public ResponseEntity<?> newsException( NewsException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

}
