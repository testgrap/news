package com.example.news.exHandler.exception;

public class AuthorException extends RuntimeException {
    public AuthorException(String message) {
        super(message);
    }
}
