package com.example.news.service;

import com.example.news.dto.NewsDto;
import com.example.news.entity.News;
import com.example.news.entity.Status;
import com.example.news.exHandler.exception.NewsException;
import com.example.news.repository.NewsRepository;
import com.example.news.util.Convertor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NewsService {

    private final NewsRepository newsRepository;

    @Transactional
    public Long saveNews(News news) {
        news.setDateTimeCreateNews(LocalDateTime.now());
        return newsRepository.save(news).getId();
    }

    @Transactional
    public NewsDto updateNews(@RequestParam(value = "id") Long id,
                              @RequestParam(value = "status") Status status) {

        News news = newsRepository.findById(id)
                .orElseThrow(() -> new NewsException("Нет новости с таким id: " + id));

        news.setStatus(status);
        news.setDateTimeCreateNews(LocalDateTime.now().withNano(0));

        return NewsDto.builder()
                .id(news.getId())
                .status(status)
                .date(news.getDateTimeCreateNews().toString()).build();
    }


    @Transactional(readOnly = true)
    public NewsDto getNews(Long id) {
        News news = newsRepository.findById(id)
                .orElseThrow(() -> new NewsException("Нет новости с таким id: " + id));

        return NewsDto.builder()
                .id(news.getId())
                .title(news.getTitle())
                .content(news.getContent())
                .date(String.valueOf(news.getDateTimeCreateNews()))
                .status(news.getStatus())
                .nameAuthor(news.getAuthor().getName())
                .uri(news.getUrlImg())
                .build();

    }

    @Transactional(readOnly = true)
    public List<NewsDto> getAllNews(Status status, LocalDateTime startPublication) {

        List<News> newsList = newsRepository.getAllByParameters(status, startPublication);
        if (newsList.size() < 1) {
            return Convertor.mappingToNewsDto(newsRepository.findAll());
        }

        return Convertor.mappingToNewsDto(newsList);
    }
}
