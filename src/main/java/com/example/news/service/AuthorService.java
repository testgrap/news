package com.example.news.service;

import com.example.news.entity.Author;
import com.example.news.exHandler.exception.AuthorException;
import com.example.news.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    @Transactional(readOnly = true)
    public Author isExistAuthor(String name) {
        return authorRepository.findByName(name)
                .orElseThrow(() -> new AuthorException("Нет автора с таким именем: " + name));
    }

    @Transactional
    public Long saveAuthor(Author author) {
        boolean isPresentAuthor = authorRepository.findByNameOrLogin(author.getName(), author.getLogin())
                .isPresent();
        if (isPresentAuthor) {
            throw new AuthorException("такой автор уже существует");
        }
        return authorRepository.save(author).getId();
    }

}